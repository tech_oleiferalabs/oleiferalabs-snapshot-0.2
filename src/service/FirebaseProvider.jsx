
import serviceAccount from "./config/firenase-conf.json";
export default class FirebaseProvider extends React.Component {

    constructor(props) {
        super(props);
        const admin = require('firebase-admin');
        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: "https://oleifera-178f9.firebaseio.com"
        });
        const db = admin.firestore(); // https://oleifera01.herokuapp.com/ | https://git.heroku.com/oleifera01.git
    }


}

